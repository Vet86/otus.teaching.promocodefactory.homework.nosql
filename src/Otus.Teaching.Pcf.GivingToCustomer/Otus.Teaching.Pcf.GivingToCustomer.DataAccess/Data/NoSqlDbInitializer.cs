﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class NoSqlDbInitializer
        : IDbInitializer
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promoCodesRepository;

        public NoSqlDbInitializer(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<PromoCode> promoCodesRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promoCodesRepository = promoCodesRepository;
        }
        
        public async Task InitializeDb()
        {
            await _customerRepository.ClearAllAsync();
            await _preferenceRepository.ClearAllAsync();
            await _promoCodesRepository.ClearAllAsync();

            var preferences = await _preferenceRepository.GetAllAsync();
            var preference = preferences.FirstOrDefault();
            if (preference is null)
            {
                preference = new Preference
                {
                    Name = "TestPreference"
                };

                await _preferenceRepository.AddAsync(preference);
            }
            
            var promoCodes = await _promoCodesRepository.GetAllAsync();
            var promoCode = promoCodes.FirstOrDefault();
            if (promoCode is null)
            {
                promoCode = new PromoCode
                {
                    BeginDate = DateTime.Now,
                    Code = "TestCode",
                    EndDate = DateTime.Now.AddYears(1),
                    Preference = preference,
                    ServiceInfo = "TestServiceInfo"
                };

                await _promoCodesRepository.AddAsync(promoCode);
            }

            var customers = await _customerRepository.GetAllAsync();
            var customer = customers.FirstOrDefault();
            if (customer is null)
            {
                customer = new Customer
                {
                    Email = "test@test.ru",
                    FirstName = "TestFirstName",
                    LastName = "TestLastName",
                    Preferences = new List<CustomerPreference>()
                    {
                        new CustomerPreference()
                        {
                            Preference = preference
                        }
                    },
                    PromoCodes = new List<PromoCodeCustomer>()
                    {
                        new PromoCodeCustomer
                        {
                            PromoCode = promoCode
                        }
                    }
                };

                await _customerRepository.AddAsync(customer);
            }

        }
    }
}