﻿using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public interface IDbInitializer
    {
        Task InitializeDb();
    }
}